<?php

use Illuminate\Database\Seeder;
use App\Movies;
class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            Movies::create([
                'poster' => $faker->image(),
                'movie' => $faker->firstName,
                'genre' => $faker->text,
                'plot' => $faker->text(200),
                'director' => $faker->name,
                'writer' => $faker->name,
                'actors' => $faker->name,
                'rating' => $faker->randomNumber(),
                'votes' => $faker->randomNumber(),
                'runtime' => $faker->time('H:s'),
            ]);
        }
    }
}
