<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/get','MoviesController@get')->name('get');
Route::post('/search','MoviesController@search')->name('search');
Route::post('/store','MoviesController@store')->name('store');
Route::get('mymovies','MoviesController@myMovies')->name('mymovies');
Route::post('/loaddata','MoviesController@loadData');
Route::post('/edit/{id}','MoviesController@edit')->name('edit');
Route::post('/update','MoviesController@update')->name('update');
Route::post('/delete/{id}','MoviesController@delete')->name('delete');