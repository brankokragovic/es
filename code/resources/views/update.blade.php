<html>

<head>

    <title>Movies</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>

<body>

<div class="container-fluid">
    {!! Form::model($movie, ['method' => 'POST', 'action' => ['MoviesController@update']]) !!}
    <input type="hidden" class="form-control" name="id" value="{{$movie->id}}">
    <label>Title:</label>
    <input type="text" class="form-control" name="movie" value="{{$movie->movie}}" readonly>
    <label>Poster:</label>
    <input type="text" class="form-control" name="poster" value="{{$movie->poster}}">
    <label>Genre:</label>
    <input type="text" class="form-control" name="genre" value="{{$movie->genre}}">
    <label>Plot:</label>
    <input type="text" class="form-control" name="plot" value="{{$movie->plot}}">
    <label>Director:</label>
    <input type="text" class="form-control" name="director" value="{{$movie->director}}">
    <label>Writer:</label>
    <input type="text" class="form-control" name="writer" value="{{$movie->writer}}">
    <label>Actors:</label>
    <input type="text" class="form-control" name="actors" value="{{$movie->actors}}">
    <label>Rating:</label>
    <input type="text" class="form-control" name="rating" value="{{$movie->rating}}">
    <label>Votes:</label>
    <input type="text" class="form-control" name="votes" value="{{$movie->votes}}">
    <label>Runtime:</label>
    <input type="text" class="form-control" name="runtime" value="{{$movie->runtime}}">

    <button class="btn-outline-primary" type="submit">Update</button>

    {!! Form::close() !!}


</div>
</body>