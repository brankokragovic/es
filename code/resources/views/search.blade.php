<html>

<head>

    <title>Movies</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>

<body>
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
@if (\Session::has('danger'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('danger') !!}</li>
        </ul>
    </div>
@endif
<button class="text-justify">
    <a href="{{ route('mymovies') }}" class="btn badge-primary">My movies</a>
</button>
<div class="container-fluid">

    <div class="row">

        <div class="panel panel-default">

            <div class="panel-heading">

                <h3>Movies </h3>

            </div>

            <div class="panel-body">

                <div class="form-group">

                    {!! Form::open(['method'=>'POST','route' => 'search','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
                    <input type="text" class="form-control" name="search" placeholder="Search...">
                    <button class="btn btn-info" type="submit">Search</button>
                    {!! Form::close() !!}

                </div>

            </div>

        </div>

    </div>

</div>
</body>

</html>