
<!DOCTYPE html>

<html>

<head>

    <title>Movies</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>

<body>

<div class="container-fluid">

    <div class="row">

        <div class="panel panel-default">

<div>
    @include('search')
</div>
                <table class="table table-bordered table-hover">

                    <thead>

                    <tr>


                    </tr>

                    </thead>

                    <tbody>

                               <th> {{ Html::image($datas['Poster'], 'Wooops :D' ,['width'=>'200','height'=>'100']) }}</th>
                               <th width="250">
                                   {{ $datas['Title']  }}
                                   <br>
                                   {{ $datas['imdbRating'] }}
                                   <br>
                                    {{$datas['Rated']}} | {{$datas['Runtime']}}
                                   <br>
                                   {{$datas['Genre']}} | {{$datas['Released']}}
                               </th>
                               <th>
                                   {{$datas['Plot']}}
                                   <br>
                                  Director: {{$datas['Director']}}
                                   <br>
                                 Writer:  {{$datas['Writer']}}
                                    <br>
                                 Actors:  {{$datas['Actors']}}
                                   <br>
                               </th>
                       <th>
                           {{ Form::open(array('url' => route('store', $datas), 'method' => 'POST')) }}
                           {!! Form::submit('Add movie', ['class' => 'btn btn-info']) !!}
                           {!! Form::close() !!}
                     </th>

                    </tbody>

                </table>

            </div>

        </div>

    </div>

</body>
