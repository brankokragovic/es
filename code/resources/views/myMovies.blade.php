<!DOCTYPE html>

<html>

<head>

    <title>Movies</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>

<body>
@if (\Session::has('danger'))
    <div class="alert alert-danger">
        <ul>
            <li>{!! \Session::get('danger') !!}</li>
        </ul>
    </div>
@endif
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="container-fluid">


    <div class="row">

        <div class="panel panel-default" style="margin-bottom: 0px">

            <div class="panel-heading">

                <h3>My movies</h3>

            </div>
            <button class="text-justify">
                <a href="{{ route('get') }}" class="btn badge-primary">Back</a>
            </button>
            <table class="table table-bordered table-hover" style="margin-bottom: 0px">

                <thead>

                <tr>
                    <th>Poster</th>
                    <th>Title</th>
                    <th>Genre</th>
                    <th>Plot</th>
                    <th>Director</th>
                    <th>Writer</th>
                    <th>Actors</th>
                    <th>Rating</th>
                    <th>Votes</th>
                    <th>Runtime</th>
                    <th>Action</th>
                </tr>

                </thead>

                <tbody>
                @foreach($movies as $movie)
                    <tr>
                        <th width="200px"> {{ Html::image($movie->poster, 'Wooops :D' ,['width'=>'200','height'=>'100' ,'center']) }}</th>
                        <th width="100px">{{$movie->movie}}</th>
                        <th width="90px">{{$movie->genre}}</th>
                        <th width="380px">{{$movie->plot}}</th>
                        <th width="70px">{{$movie->director}}</th>
                        <th width="170px">{{$movie->writer}}</th>
                        <th width="120px">{{$movie->actors}}</th>
                        <th width="40px">{{$movie->rating}}</th>
                        <th width="60px">{{$movie->votes}}</th>
                        <th width="70px">{{$movie->runtime}}</th>
                        <th width="70px">
                            {!! Form::model($movie, ['method' => 'POST', 'action' => ['MoviesController@edit',$movie->id]]) !!}
                            <input type="hidden" class="form-control">
                            <button class="glyphicon glyphicon-pencil" type="submit">EDIT</button>
                            {!! Form::close() !!}

                            {!! Form::model($movie, ['method' => 'POST', 'action' => ['MoviesController@delete',$movie->id]]) !!}
                            <input type="hidden" class="form-control">
                            <button class="glyphicon glyphicon-trash" type="submit">Delete</button>
                            {!! Form::close() !!}
                        </th>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>

    </div>

</div>

<script>
    $(document).ready(function () {
        $(document).on('click', '#btn-more', function () {
            var id = $(this).data('id');
            $("#btn-more").html("Loading....");

            $.ajax({
                url: '{{ url("/loaddata") }}',
                method: "POST",
                data: {id: id, _token: "{{csrf_token()}}"},
                dataType: "text",
                success: function (data) {
                    if (data) {
                        $('#remove-row').remove();
                        $('#load-data').append(data);
                    }
                    else {
                        $('#btn-more').html("No Data");
                    }
                }
            });
        });
    });
</script>
<div id="remove-row">
    <button id="btn-more" data-id={{$movie->id}} class="nounderline btn-block mdl-button mdl-js-button
            mdl-button--raised mdl-js-ripple-effect mdl-button--accent
    " > Load More </button>
</div>
<div id="load-data">

</div>
</body>
</html>
