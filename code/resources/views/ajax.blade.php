<html>

<head>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

</head>

<body>
<div class="container-fluid">

    <div class="row">

            <table class="table table-bordered table-hover" style="margin-bottom: 0px">

                <tbody>
                @foreach($movies as $movie)
                    <tr>
                        <th width="195px"> {{ Html::image($movie->poster, 'Wooops :D' ,['width'=>'200','height'=>'100','center'] ) }}</th>
                        <th width="93px">{{$movie->movie}}</th>
                        <th width="90px">{{$movie->genre}}</th>
                        <th width="380px">{{$movie->plot}}</th>
                        <th width="70px">{{$movie->director}}</th>
                        <th width="170px">{{$movie->writer}}</th>
                        <th width="120px">{{$movie->actors}}</th>
                        <th width="40px">{{$movie->rating}}</th>
                        <th width="60px">{{$movie->votes}}</th>
                        <th width="70px">{{$movie->runtime}}</th>
                        <th width="70px">
                            {!! Form::model($movie, ['method' => 'POST', 'action' => ['MoviesController@edit',$movie->id]]) !!}
                            <input type="hidden" class="form-control"><button class="glyphicon glyphicon-pencil" type="submit">EDIT</button>
                            {!! Form::close() !!}

                            {!! Form::model($movie, ['method' => 'POST', 'action' => ['MoviesController@delete',$movie->id]]) !!}
                            <input type="hidden" class="form-control">
                            <button class="glyphicon glyphicon-trash" type="submit">Delete</button>
                            {!! Form::close() !!}
                        </th>
                    </tr>
                @endforeach
                </tbody>

            </table>

        </div>
    </div>
</body>