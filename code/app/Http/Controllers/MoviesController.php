<?php

namespace App\Http\Controllers;

use App\Movies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\DeclareDeclare;

class MoviesController extends Controller
{

    public function search(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $request = $client->get('http://www.omdbapi.com/?t=' . $request->search . '&apikey=c402a52a');
        $response = $request->getBody()->getContents();

        $datas = json_decode($response, true);
        if (count($datas) > 3) {
            return view('list', compact('datas', $datas));
        } else {
            return redirect()->to('/get')->with('danger', 'Movie not exist');
        }
    }


    public function get(Request $request)
    {
        return view('search');
    }

    public function store(Request $request)
    {
        if (DB::table('movies')->where(['movie' => $request['Title']])->exists()) {
            return redirect()->to('/get')->with('danger', 'This movie already exist in your list');
        } else {
            $movie = new Movies();
            $movie->poster = $request['Poster'];
            $movie->movie = $request['Title'];
            $movie->genre = $request['Genre'];
            $movie->plot = $request['Plot'];
            $movie->director = $request['Director'];
            $movie->writer = $request['Writer'];
            $movie->actors = $request['Actors'];
            $movie->rating = $request['imdbRating'];
            $movie->votes = $request['imdbVotes'];
            $movie->runtime = $request['Runtime'];
            $movie->save();

            return redirect()->to('/mymovies')->with('success', 'Movie saved');
        }
    }

    public function loadData(Request $request)
    {
        $id = $request->id;
        $movies = DB::table('movies')->where('id', '>', $id)->limit(5)->get();

        return view('ajax', compact('movies', $movies));
    }

    public function myMovies()
    {

        $movies = DB::table('movies')->orderBy('id','DESC')->limit(5)->get();

        return view('myMovies', compact('movies', $movies));
    }

    public function edit($id)
    {

        $movie = Movies::find($id);

        return view('update', compact('movie', $movie));
    }

    public function update(Request $request)
    {

        $id = $request->id;
        $movie = Movies::find($id);
        $movie->poster = $request->poster;
        $movie->movie = $request->movie;
        $movie->genre = $request->genre;
        $movie->plot = $request->plot;
        $movie->director = $request->director;
        $movie->writer = $request->writer;
        $movie->actors = $request->actors;
        $movie->rating = $request->rating;
        $movie->votes = $request->votes;
        $movie->runtime = $request->runtime;
        $movie->save();

        return redirect()->to('mymovies')->with('success', 'Movie updated');
    }

    public function delete($id)
    {
        $movie = Movies::find($id);
        $movie->delete();

        return redirect()->to('/mymovies')->with('danger', 'Movie deleted');

    }
}
