
FROM php:7.1-fpm

RUN apt-get update && \
   apt-get install -y \
       zlib1g-dev

RUN echo "deb http://www.deb-multimedia.org jessie main non-free" >>/etc/apt/sources.list
RUN echo "deb-src http://www.deb-multimedia.org jessie main non-free" >>/etc/apt/sources.list
RUN apt-get update && apt-get install -y --force-yes deb-multimedia-keyring

RUN apt-get update && apt-get install -y \
       wget \
       build-essential \
       libmp3lame-dev \
       libvorbis-dev \
       libtheora-dev \
       libspeex-dev \
       yasm \
       pkg-config \
       libfaac-dev \
       libx264-dev \
       libfreetype6 \
       libfreetype6-dev \
       libfribidi-dev \
       libfontconfig1-dev \
       libcurl4-openssl-dev \
       libssl-dev \
       libxrender1 \
       libxext6 \
       pkg-config \
       software-properties-common


       RUN apt-get update && apt-get install -y \
       libfreetype6-dev \
       libmcrypt-dev \
       libjpeg-dev \
       libpng-dev \
       vim \
   && docker-php-ext-install iconv mcrypt \
   && docker-php-ext-configure gd \
       --enable-gd-native-ttf \
       --with-freetype-dir=/usr/include/freetype2 \
       --with-png-dir=/usr/include \
       --with-jpeg-dir=/usr/include \
   && docker-php-ext-install gd \
   && docker-php-ext-install mbstring \
   && docker-php-ext-enable gd


RUN pecl install redis-3.1.4 \
   && docker-php-ext-enable redis
RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get install -y zlib1g-dev && rm -rf /var/lib/apt/lists/* && docker-php-ext-install zip

RUN apt update && apt install curl && \
   curl -sS https://getcomposer.org/installer | php \
   && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer